-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-09-2015 a las 00:49:55
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cine`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE IF NOT EXISTS `factura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) NOT NULL,
  `idPelicula` int(11) NOT NULL,
  `puestos` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(10) NOT NULL,
  `total` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id`, `idUsuario`, `idPelicula`, `puestos`, `cantidad`, `fecha`, `hora`, `total`) VALUES
(1, 1, 1, 'a:0:{}', 0, '2015-02-01', '01:21:08', '0'),
(2, 1, 1, 'a:2:{i:0;i:0;i:1;i:44;}', 2, '2015-02-01', '01:21:09', '24'),
(3, 1, 1, 'a:2:{i:0;i:4;i:1;i:14;}', 2, '2015-02-01', '02:00:17', '24'),
(4, 1, 1, 'a:2:{i:0;i:4;i:1;i:14;}', 2, '2015-02-01', '02:02:02', '24'),
(5, 1, 1, 'a:1:{i:0;i:24;}', 1, '2015-02-01', '02:02:03', '12'),
(6, 1, 1, 'a:1:{i:0;i:33;}', 1, '2015-02-01', '02:02:33', '12'),
(7, 1, 1, 'a:1:{i:0;i:10;}', 1, '2015-02-01', '02:07:59', '0'),
(8, 1, 1, 'a:1:{i:0;i:10;}', 1, '2015-02-01', '02:08:08', '0'),
(9, 1, 1, 'a:1:{i:0;i:40;}', 1, '2015-02-01', '02:08:48', '0'),
(10, 1, 1, 'a:1:{i:0;i:5;}', 1, '2015-02-01', '02:11:57', '12'),
(11, 1, 1, 'a:1:{i:0;i:5;}', 1, '2015-02-01', '02:12:50', '12'),
(12, 1, 1, 'a:1:{i:0;i:9;}', 1, '2015-02-01', '02:12:51', '12'),
(13, 1, 1, 'a:1:{i:0;i:5;}', 1, '2015-02-01', '02:13:11', '12'),
(14, 1, 1, 'a:1:{i:0;i:9;}', 1, '2015-02-01', '02:13:13', '12'),
(15, 1, 1, 'a:1:{i:0;i:27;}', 1, '2015-02-01', '02:13:14', '12'),
(16, 1, 1, 'a:1:{i:0;i:5;}', 1, '2015-02-01', '02:13:43', '12'),
(17, 1, 1, 'a:1:{i:0;i:9;}', 1, '2015-02-01', '02:13:44', '12'),
(18, 1, 1, 'a:1:{i:0;i:27;}', 1, '2015-02-01', '02:13:45', '12'),
(19, 1, 1, 'a:1:{i:0;i:45;}', 1, '2015-02-01', '02:13:47', '12'),
(20, 1, 1, 'a:1:{i:0;i:5;}', 1, '2015-02-01', '02:23:27', '12'),
(21, 1, 1, 'a:1:{i:0;i:9;}', 1, '2015-02-01', '02:23:29', '12'),
(22, 1, 1, 'a:1:{i:0;i:27;}', 1, '2015-02-01', '02:23:30', '12'),
(23, 1, 1, 'a:1:{i:0;i:45;}', 1, '2015-02-01', '02:23:32', '12'),
(24, 1, 1, 'a:1:{i:0;i:17;}', 1, '2015-02-01', '02:23:33', '12'),
(25, 1, 1, 'a:1:{i:0;i:47;}', 1, '2015-02-01', '02:28:44', '12'),
(26, 1, 1, 'a:0:{}', 0, '2015-02-24', '07:04:10', '0'),
(27, 1, 1, 'a:1:{i:0;i:16;}', 1, '2015-02-24', '07:04:11', '12'),
(28, 1, 2, 'a:5:{i:0;i:0;i:1;i:1;i:2;i:2;i:3;i:3;i:4;i:4;}', 5, '2015-02-24', '07:04:12', '250'),
(29, 1, 2, 'a:5:{i:0;i:0;i:1;i:1;i:2;i:2;i:3;i:3;i:4;i:4;}', 5, '2015-02-24', '07:04:14', '250');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE IF NOT EXISTS `pelicula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `genero` varchar(200) NOT NULL,
  `duracion` varchar(4) NOT NULL,
  `descripcion` text NOT NULL,
  `clasificacion` varchar(1) NOT NULL,
  `idSala` int(11) NOT NULL,
  `hora` varchar(1) NOT NULL,
  `fechadeinicio` date NOT NULL,
  `fechafinal` date NOT NULL,
  `costo` varchar(10) NOT NULL,
  `coverimagen` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`id`, `nombre`, `genero`, `duracion`, `descripcion`, `clasificacion`, `idSala`, `hora`, `fechadeinicio`, `fechafinal`, `costo`, `coverimagen`) VALUES
(1, 'Xmens modificado', 'a:1:{i:0;s:8:"Misterio";}', '60', 'modificado', 'A', 8, '3', '2015-01-01', '2019-04-01', '120', 'Coverc4ca4238a0b923820dcc509a6f75849b.png'),
(2, 'Rapido y Furioso 7', 'a:1:{i:0;s:7:"acciÃ³n";}', '90', 'sdf', 'A', 1, '1', '2015-02-11', '2019-02-28', '50', 'Coverc81e728d9d4c2f636f067f89cc14862c.jpg'),
(3, '50 sombras de grey', 'a:3:{i:0;s:5:"Drama";i:1;s:8:"ErÃ³tico";i:2;s:12:"Dramaromance";}', '90', '50 sombras de grey en accion', 'C', 1, '1', '2015-02-25', '2019-03-25', '140', 'Covereccbc87e4b5ce2fe28308fd9f2a7baf3.jpg'),
(4, 'En el Bosquet', 'a:4:{i:0;s:10:"animaciÃ³n";i:1;s:15:"CienciaFicciÃ³n";i:2;s:15:"Dibujosanimados";i:3;s:8:"Infantil";}', '80', 'De calidad', 'A', 2, '2', '2015-02-26', '2019-03-26', '140', 'Covera87ff679a2f3e71d9181a67b7542122c.jpg'),
(5, 'El Libro de la vidaa', 'a:4:{i:0;s:10:"animaciÃ³n";i:1;s:15:"CienciaFicciÃ³n";i:2;s:15:"Dibujosanimados";i:3;s:8:"Infantil";}', '90', 'Infantil', 'A', 9, '1', '2015-02-01', '2019-04-01', '150', 'Covere4da3b7fbbce2345d7772b0674a318d5.jpg'),
(6, 'The Amazing Spiderman 2', 'a:1:{i:0;s:7:"acciÃ³n";}', '90', 'Hombre AraÃ±a', 'B', 4, '3', '2015-02-01', '2019-03-01', '150', 'Cover1679091c5a880faf6fb5e6087eb1b2dc.jpg'),
(7, 'Whiplash', 'a:1:{i:0;s:7:"Musical";}', '90', 'Accion', 'A', 1, '1', '2015-09-01', '2015-09-30', '5', 'Cover8f14e45fceea167a5a36dedd4bea2543.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sala`
--

CREATE TABLE IF NOT EXISTS `sala` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `maximo` varchar(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `numero` (`numero`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `sala`
--

INSERT INTO `sala` (`id`, `numero`, `nombre`, `maximo`) VALUES
(1, '1', 'Uno', '100'),
(2, '5', 'cinco', ''),
(4, '6', 'cinco', ''),
(7, '8', 'ocho', ''),
(8, '10', '4d', ''),
(9, '11', 'Once', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sillas`
--

CREATE TABLE IF NOT EXISTS `sillas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPelicula` int(11) NOT NULL,
  `disponible` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idPelicula` (`idPelicula`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `sillas`
--

INSERT INTO `sillas` (`id`, `idPelicula`, `disponible`) VALUES
(1, 1, 'a:50:{i:0;s:1:"1";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";i:4;s:1:"1";i:5;s:1:"1";i:6;s:1:"0";i:7;s:1:"0";i:8;s:1:"0";i:9;s:1:"1";i:10;s:1:"1";i:11;s:1:"0";i:12;s:1:"0";i:13;s:1:"0";i:14;s:1:"1";i:15;s:1:"0";i:16;i:1;i:17;s:1:"1";i:18;s:1:"0";i:19;s:1:"0";i:20;s:1:"0";i:21;s:1:"0";i:22;s:1:"0";i:23;s:1:"0";i:24;s:1:"1";i:25;s:1:"0";i:26;s:1:"0";i:27;s:1:"1";i:28;s:1:"0";i:29;s:1:"0";i:30;s:1:"0";i:31;s:1:"0";i:32;s:1:"0";i:33;s:1:"1";i:34;s:1:"0";i:35;s:1:"0";i:36;s:1:"0";i:37;s:1:"0";i:38;s:1:"0";i:39;s:1:"0";i:40;s:1:"1";i:41;s:1:"0";i:42;s:1:"0";i:43;s:1:"0";i:44;s:1:"1";i:45;s:1:"1";i:46;s:1:"0";i:47;s:1:"1";i:48;s:1:"0";i:49;s:1:"0";}'),
(2, 2, 'a:50:{i:0;i:1;i:1;i:1;i:2;i:1;i:3;i:1;i:4;i:1;i:5;s:1:"0";i:6;s:1:"0";i:7;s:1:"0";i:8;s:1:"0";i:9;s:1:"0";i:10;s:1:"0";i:11;s:1:"0";i:12;s:1:"0";i:13;s:1:"0";i:14;s:1:"0";i:15;s:1:"0";i:16;s:1:"0";i:17;s:1:"0";i:18;s:1:"0";i:19;s:1:"0";i:20;s:1:"0";i:21;s:1:"0";i:22;s:1:"0";i:23;s:1:"0";i:24;s:1:"0";i:25;s:1:"0";i:26;s:1:"0";i:27;s:1:"0";i:28;s:1:"0";i:29;s:1:"0";i:30;s:1:"0";i:31;s:1:"0";i:32;s:1:"0";i:33;s:1:"0";i:34;s:1:"0";i:35;s:1:"0";i:36;s:1:"0";i:37;s:1:"0";i:38;s:1:"0";i:39;s:1:"0";i:40;s:1:"0";i:41;s:1:"0";i:42;s:1:"0";i:43;s:1:"0";i:44;s:1:"0";i:45;s:1:"0";i:46;s:1:"0";i:47;s:1:"0";i:48;s:1:"0";i:49;s:1:"0";}'),
(3, 3, 'a:50:{i:0;i:0;i:1;i:0;i:2;i:0;i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;i:10;i:0;i:11;i:0;i:12;i:0;i:13;i:0;i:14;i:0;i:15;i:0;i:16;i:0;i:17;i:0;i:18;i:0;i:19;i:0;i:20;i:0;i:21;i:0;i:22;i:0;i:23;i:0;i:24;i:0;i:25;i:0;i:26;i:0;i:27;i:0;i:28;i:0;i:29;i:0;i:30;i:0;i:31;i:0;i:32;i:0;i:33;i:0;i:34;i:0;i:35;i:0;i:36;i:0;i:37;i:0;i:38;i:0;i:39;i:0;i:40;i:0;i:41;i:0;i:42;i:0;i:43;i:0;i:44;i:0;i:45;i:0;i:46;i:0;i:47;i:0;i:48;i:0;i:49;i:0;}'),
(4, 4, 'a:50:{i:0;i:0;i:1;i:0;i:2;i:0;i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;i:10;i:0;i:11;i:0;i:12;i:0;i:13;i:0;i:14;i:0;i:15;i:0;i:16;i:0;i:17;i:0;i:18;i:0;i:19;i:0;i:20;i:0;i:21;i:0;i:22;i:0;i:23;i:0;i:24;i:0;i:25;i:0;i:26;i:0;i:27;i:0;i:28;i:0;i:29;i:0;i:30;i:0;i:31;i:0;i:32;i:0;i:33;i:0;i:34;i:0;i:35;i:0;i:36;i:0;i:37;i:0;i:38;i:0;i:39;i:0;i:40;i:0;i:41;i:0;i:42;i:0;i:43;i:0;i:44;i:0;i:45;i:0;i:46;i:0;i:47;i:0;i:48;i:0;i:49;i:0;}'),
(5, 5, 'a:50:{i:0;i:0;i:1;i:0;i:2;i:0;i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;i:10;i:0;i:11;i:0;i:12;i:0;i:13;i:0;i:14;i:0;i:15;i:0;i:16;i:0;i:17;i:0;i:18;i:0;i:19;i:0;i:20;i:0;i:21;i:0;i:22;i:0;i:23;i:0;i:24;i:0;i:25;i:0;i:26;i:0;i:27;i:0;i:28;i:0;i:29;i:0;i:30;i:0;i:31;i:0;i:32;i:0;i:33;i:0;i:34;i:0;i:35;i:0;i:36;i:0;i:37;i:0;i:38;i:0;i:39;i:0;i:40;i:0;i:41;i:0;i:42;i:0;i:43;i:0;i:44;i:0;i:45;i:0;i:46;i:0;i:47;i:0;i:48;i:0;i:49;i:0;}'),
(6, 6, 'a:50:{i:0;i:0;i:1;i:0;i:2;i:0;i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;i:10;i:0;i:11;i:0;i:12;i:0;i:13;i:0;i:14;i:0;i:15;i:0;i:16;i:0;i:17;i:0;i:18;i:0;i:19;i:0;i:20;i:0;i:21;i:0;i:22;i:0;i:23;i:0;i:24;i:0;i:25;i:0;i:26;i:0;i:27;i:0;i:28;i:0;i:29;i:0;i:30;i:0;i:31;i:0;i:32;i:0;i:33;i:0;i:34;i:0;i:35;i:0;i:36;i:0;i:37;i:0;i:38;i:0;i:39;i:0;i:40;i:0;i:41;i:0;i:42;i:0;i:43;i:0;i:44;i:0;i:45;i:0;i:46;i:0;i:47;i:0;i:48;i:0;i:49;i:0;}'),
(7, 7, 'a:50:{i:0;i:0;i:1;i:0;i:2;i:0;i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:7;i:0;i:8;i:0;i:9;i:0;i:10;i:0;i:11;i:0;i:12;i:0;i:13;i:0;i:14;i:0;i:15;i:0;i:16;i:0;i:17;i:0;i:18;i:0;i:19;i:0;i:20;i:0;i:21;i:0;i:22;i:0;i:23;i:0;i:24;i:0;i:25;i:0;i:26;i:0;i:27;i:0;i:28;i:0;i:29;i:0;i:30;i:0;i:31;i:0;i:32;i:0;i:33;i:0;i:34;i:0;i:35;i:0;i:36;i:0;i:37;i:0;i:38;i:0;i:39;i:0;i:40;i:0;i:41;i:0;i:42;i:0;i:43;i:0;i:44;i:0;i:45;i:0;i:46;i:0;i:47;i:0;i:48;i:0;i:49;i:0;}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario2`
--

CREATE TABLE IF NOT EXISTS `usuario2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(25) NOT NULL,
  `Apellido` varchar(25) NOT NULL,
  `Fecha` date NOT NULL,
  `Pais` varchar(50) NOT NULL,
  `Estados` varchar(50) NOT NULL,
  `Sexo` varchar(1) NOT NULL,
  `Direccion` varchar(200) NOT NULL,
  `Telefono` varchar(12) NOT NULL,
  `Correo` varchar(75) NOT NULL,
  `Pass` varchar(50) NOT NULL,
  `Pregunta` varchar(50) NOT NULL,
  `Respuesta` varchar(50) NOT NULL,
  `privilegio` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Correo` (`Correo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `usuario2`
--

INSERT INTO `usuario2` (`id`, `Nombre`, `Apellido`, `Fecha`, `Pais`, `Estados`, `Sexo`, `Direccion`, `Telefono`, `Correo`, `Pass`, `Pregunta`, `Respuesta`, `privilegio`) VALUES
(1, 'Javier Antonio2', 'Delgado Alfonzo', '2014-11-12', 'Venezuela', 'Amazonas', 'R', 'Valle Verde', '04248309075', 'cheche338@gmail.com', '123', 'Mi novia?', 'yeimy', 0),
(3, 'Javier Antonio', 'Delgado Alfonzo', '2014-11-22', 'Venezuela', 'Amazonas', 'R', 'Valle Verde', '04248309075', 'admin@admin.com', '123', 'Mi novia?', 'yeimy', 1),
(4, 'Javier Antonio', 'Delgado Alfonzo', '2014-11-25', 'Venezuela', 'Amazonas', 'R', 'Valle Verde', '04248309075', 'chenche338@gmail.com', '123', 'Mi novia?', 'yeimy', 0),
(5, 'Javier Antonio', 'Delgado Alfonzo', '2014-11-28', 'Venezuela', 'Amazonas', 'R', 'Valle Verde', '04248309075', 'chechfdse338@gmail.com', '123', 'Mi novia?', 'yeimy', 0),
(6, 'Javier Antonio', 'Delgado Alfonzo', '2014-12-04', 'Venezuela', 'Amazonas', 'R', 'Valle Verde', '04248309075', 'javier@gmail.com', '123', 'Mi novia?', 'yeimy', 0),
(7, 'Javier', 'delgado', '2015-02-25', 'Venezuela', 'Anzoategui', 'R', 'Valle Verde Calle Principal', '584148309075', 'cheche338@hotmail.com', '123', 'mi nombre', 'javier', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
