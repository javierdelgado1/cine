<?php

	include "Pelicula.php";
	$id=$_REQUEST['id'];
	switch($id)
	{
		case 1:
			$p=new  Pelicula();
			$p->ObtenerPeliculas();
			break;
		case 2:
			$p=new Pelicula();
			$p->Agregar($_REQUEST['nombre'], $_REQUEST['genero'], $_REQUEST['duracion'], $_REQUEST['descripcion'], $_REQUEST['clasificacion'], $_REQUEST['sala'], $_REQUEST['hora'], $_REQUEST['fecha1'], $_REQUEST['fecha2'], $_REQUEST['costo']);
			break;
		case 3:
			$p=new Pelicula();
			$p->ObtenerSillasDisponibles($_REQUEST['ID']);
			break;
		case 4:
			$p=new Pelicula();
			$p->ComprarSilla($_REQUEST['ID'], $_REQUEST['puestos']);
			break;
		case 5:
			$p=new Pelicula();
			$p->BuscarPelicula($_REQUEST['data']);
			break;
		case 6:
			$p=new Pelicula();
			$p->BuscarPeliculaAdministrador($_REQUEST['data']);
			break;
		case 7:
			$p=new Pelicula();
			$p->ObtenerPeliculasSegunID($_REQUEST['ID']);
			break;
		case 8:
			$p=new Pelicula();
			$p->ObtenerFactura();
			break;
		case 9:
			$p=new Pelicula();
			$p->ObtenerFacturaEspecifica($_REQUEST['ID']);
			break;
		case 10: 
			$p=new Pelicula();
			$p->ObtenerCartelera();
			break;
		case 11:
			$p=new Pelicula();
			$p->obtenerProximosEstrenos();
			break;
		case 12:
			$p=new Pelicula();
			$p->ObtenerDetallesdeLaPelicula($_REQUEST['ID']);
			break;
		case 13:
			$p=new Pelicula();
			$p->ActualizarPelicula($_REQUEST['nombre'], $_REQUEST['genero'], $_REQUEST['duracion'], $_REQUEST['descripcion'], $_REQUEST['clasificacion'], $_REQUEST['sala'], $_REQUEST['hora'], $_REQUEST['fecha1'], $_REQUEST['fecha2'], $_REQUEST['costo'], $_REQUEST['idpelicula']);
			break;
		default;
	}
?>