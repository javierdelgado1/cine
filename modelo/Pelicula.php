<?php
	include("conector.php");
	class Pelicula {
		function __construct() {
	       
		}

		function ActualizarPelicula($nombre, $genero, $duracion, $descripcion, $clasificacion, $sala, $hora, $fecha1, $fecha2, $costo, $idPelicula){
			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			$this->salida="true";
			$genero=serialize($genero);
			$this->tupla="UPDATE  pelicula SET nombre='$nombre', genero='$genero', duracion='$duracion', descripcion='$descripcion', clasificacion='$clasificacion',	idSala='$sala', hora='$hora', fechadeinicio='$fecha1', fechafinal='$fecha2', costo='$costo' WHERE id ='$idPelicula' ";
			$this->resultado = $this->mysqli->query($this->tupla) or $salida=$mysqli->error;

			$this->mysqli->close();
			echo json_encode($this->salida);
		}
		function ObtenerDetallesdeLaPelicula($id){
			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			$this->tupla="SELECT pelicula.*, sala.nombre as NombreSala FROM `pelicula` INNER JOIN  sala  on sala.id=pelicula.idSala WHERE  pelicula.id='$id' ORDER BY id DESC";
			$this->resultado = $this->mysqli->query($this->tupla);
			$this->objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$this->objeto[$this->i]['id']=$this->db_resultado['id'];
				$this->objeto[$this->i]['sala']=$this->db_resultado['NombreSala'];
				$this->objeto[$this->i]['nombre']=$this->db_resultado['nombre'];
				$this->objeto[$this->i]['genero']=unserialize($this->db_resultado['genero']);
				$this->objeto[$this->i]['duracion']=$this->db_resultado['duracion'];
				$this->objeto[$this->i]['descripcion']=$this->db_resultado['descripcion'];
				$this->objeto[$this->i]['clasificacion']=$this->db_resultado['clasificacion'];
				$this->objeto[$this->i]['idSala']=$this->db_resultado['idSala'];
				$this->objeto[$this->i]['hora']=$this->db_resultado['hora'];
				$this->objeto[$this->i]['fechadeinicio']=$this->db_resultado['fechadeinicio'];
				$date = new DateTime($this->objeto[$this->i]['fechadeinicio']);
				$this->objeto[$this->i]['fechadeinicio']=$date->format('d-m-Y');
				$this->objeto[$this->i]['fechafinal']=$this->db_resultado['fechafinal'];
				$date = new DateTime($this->objeto[$this->i]['fechafinal']);
			
				$this->objeto[$this->i]['costo']=$this->db_resultado['costo'];
				
			}
			$this->mysqli->close();
			echo json_encode($this->objeto);
		}
		function obtenerProximosEstrenos(){
			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			$this->fecha=date("Y-m-d");
			$this->tupla="SELECT * FROM `pelicula` WHERE fechadeinicio>='$this->fecha' ORDER BY id Desc";

			$this->resultado = $this->mysqli->query($this->tupla);
			$this->objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			
			while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$this->objeto[$this->i]['id']=$this->db_resultado['id'];
				$this->objeto[$this->i]['nombre']=$this->db_resultado['nombre'];
				$this->objeto[$this->i]['genero']=unserialize($this->db_resultado['genero']);
				$this->objeto[$this->i]['duracion']=$this->db_resultado['duracion'];
				$this->objeto[$this->i]['descripcion']=$this->db_resultado['descripcion'];
				$this->objeto[$this->i]['clasificacion']=$this->db_resultado['clasificacion'];
				$this->objeto[$this->i]['idSala']=$this->db_resultado['idSala'];
				$this->objeto[$this->i]['hora']=$this->db_resultado['hora'];
				$this->objeto[$this->i]['fechadeinicio']=$this->db_resultado['fechadeinicio'];
				/*$date = new DateTime($this->objeto[$this->i]['fechadeinicio']);
				$this->objeto[$this->i]['fechadeinicio']=$date->format('d-m-Y');*/
				$this->objeto[$this->i]['fechafinal']=$this->db_resultado['fechafinal'];
			/*	$date = new DateTime($this->objeto[$this->i]['fechafinal']);
				$this->objeto[$this->i]['fechafinal']=$date->format('d-m-Y');*/
				$this->objeto[$this->i]['costo']=$this->db_resultado['costo'];
				$this->objeto[$this->i]['idPelicula']=md5($this->db_resultado['id']);
				$this->objeto[$this->i]['cover']=$this->db_resultado['coverimagen'];
				/*if ($directorio = opendir("uploads/".$this->objeto[$this->i]['idPelicula']));
				{
					$j=0;
					 while (false !== ($entrada = readdir($directorio))) { 		
					        if($j==2)
					        	{
					        		$this->objeto[$this->i]['cover']=$entrada;*/

					/*    		}
					        $j++;
					}
					closedir($directorio);
				}
*/
				$this->i++;
			}
			$this->mysqli->close();
			echo json_encode($this->objeto);
		}
		function ObtenerCartelera(){

			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			$this->fecha=date("Y-m-d");
			$this->tupla="SELECT * FROM `pelicula` WHERE '$this->fecha'>=fechadeinicio and '$this->fecha'<=fechafinal ORDER BY id Desc";

			$this->resultado = $this->mysqli->query($this->tupla);
			$this->objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			session_start();
			if(isset($_SESSION['id'])){
				$this->objeto[$this->i]['sesion']="true";

			}
			else{
				$this->objeto[$this->i]['sesion']="false";

			}
			while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$this->objeto[$this->i]['id']=$this->db_resultado['id'];
				$this->objeto[$this->i]['nombre']=$this->db_resultado['nombre'];
				$this->objeto[$this->i]['genero']=unserialize($this->db_resultado['genero']);
				$this->objeto[$this->i]['duracion']=$this->db_resultado['duracion'];
				$this->objeto[$this->i]['descripcion']=$this->db_resultado['descripcion'];
				$this->objeto[$this->i]['clasificacion']=$this->db_resultado['clasificacion'];
				$this->objeto[$this->i]['idSala']=$this->db_resultado['idSala'];
				$this->objeto[$this->i]['hora']=$this->db_resultado['hora'];
				$this->objeto[$this->i]['fechadeinicio']=$this->db_resultado['fechadeinicio'];
				/*$date = new DateTime($this->objeto[$this->i]['fechadeinicio']);
				$this->objeto[$this->i]['fechadeinicio']=$date->format('d-m-Y');*/
				$this->objeto[$this->i]['fechafinal']=$this->db_resultado['fechafinal'];
			/*	$date = new DateTime($this->objeto[$this->i]['fechafinal']);
				$this->objeto[$this->i]['fechafinal']=$date->format('d-m-Y');*/
				$this->objeto[$this->i]['costo']=$this->db_resultado['costo'];
				$this->objeto[$this->i]['idPelicula']=md5($this->db_resultado['id']);
				$this->objeto[$this->i]['cover']=$this->db_resultado['coverimagen'];
				/*if ($directorio = opendir("uploads/".$this->objeto[$this->i]['idPelicula']));
				{
					$j=0;
					 while (false !== ($entrada = readdir($directorio))) { 		
					        if($j==2)
					        	{$this->objeto[$this->i]['cover']=$entrada;

					    		}
					        $j++;
					}
					closedir($directorio);
				}*/

				$this->i++;
			}
			$this->mysqli->close();
			echo json_encode($this->objeto);
		}
		function ObtenerFacturaEspecifica($id){
			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			

			$this->tupla="SELECT factura.*,  pelicula.nombre as pelicula, sala.numero, sala.nombre as nombreSala, pelicula.hora as hora2, usuario2.Nombre, usuario2.Apellido  FROM  factura INNER JOIN  pelicula on factura.idPelicula=pelicula.id INNER JOIN  sala on pelicula.idSala=sala.id INNER JOIN usuario2 on factura.idUsuario=usuario2.id  WHERE  factura.id='$id' ";
			$this->resultado = $this->mysqli->query($this->tupla);
			$this->objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$this->objeto[$this->i]['id']=$this->db_resultado['id'];
				$this->objeto[$this->i]['nombrePelicula']=$this->db_resultado['pelicula'];
				$this->objeto[$this->i]['hora']=$this->db_resultado['hora'];
				$this->objeto[$this->i]['Fecha']=$this->db_resultado['fecha'];
				$this->objeto[$this->i]['numero']=$this->db_resultado['numero'];
				$this->objeto[$this->i]['asientos']=unserialize($this->db_resultado['puestos']);
				$this->objeto[$this->i]['cantidad']=$this->db_resultado['cantidad'];
				$this->objeto[$this->i]['total']=$this->db_resultado['total'];	
				$this->objeto[$this->i]['nombre']=$this->db_resultado['Nombre']." ".$this->db_resultado['Apellido'];
				$this->objeto[$this->i]['sala']=$this->db_resultado['nombreSala'];
				$this->objeto[$this->i]['hora2']=$this->db_resultado['hora2'];	



			
				
			}
			$this->mysqli->close();
			echo json_encode($this->objeto);

		}
		function ObtenerFactura(){
			session_start();
			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			$this->idUsuario=$_SESSION['id'];

			$this->tupla="SELECT factura.*, factura.id as idFactura, pelicula.nombre, sala.numero  FROM  factura INNER JOIN  pelicula on factura.idPelicula=pelicula.id INNER JOIN  sala on pelicula.idSala=sala.id  WHERE  factura.idUsuario='$this->idUsuario' ORDER BY factura.id DESC";
			$this->resultado = $this->mysqli->query($this->tupla);
			$this->objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			
			while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$this->objeto[$this->i]['id']=$this->db_resultado['id'];
				$this->objeto[$this->i]['nombre']=$this->db_resultado['nombre'];
				$this->objeto[$this->i]['hora']=$this->db_resultado['hora'];
				$this->objeto[$this->i]['fecha']=$this->db_resultado['fecha'];
				$this->objeto[$this->i]['numero']=$this->db_resultado['numero'];
				
				$this->i++;
				
			}
			$this->mysqli->close();
			echo json_encode($this->objeto);
		}
		function ObtenerPeliculasSegunID($ID){
			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			$this->tupla="SELECT * FROM pelicula WHERE id='$ID'";
			$this->resultado = $this->mysqli->query($this->tupla);
			$this->objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$this->objeto[$this->i]['id']=$this->db_resultado['id'];
				$this->objeto[$this->i]['nombre']=$this->db_resultado['nombre'];
				$this->objeto[$this->i]['genero']=unserialize($this->db_resultado['genero']);
				$this->objeto[$this->i]['duracion']=$this->db_resultado['duracion'];
				$this->objeto[$this->i]['descripcion']=$this->db_resultado['descripcion'];
				$this->objeto[$this->i]['clasificacion']=$this->db_resultado['clasificacion'];
				$this->objeto[$this->i]['idSala']=$this->db_resultado['idSala'];
				$this->objeto[$this->i]['hora']=$this->db_resultado['hora'];
				$this->objeto[$this->i]['fechadeinicio']=$this->db_resultado['fechadeinicio'];
				/*$date = new DateTime($this->objeto[$this->i]['fechadeinicio']);
				$this->objeto[$this->i]['fechadeinicio']=$date->format('d-m-Y');*/
				$this->objeto[$this->i]['fechafinal']=$this->db_resultado['fechafinal'];
			/*	$date = new DateTime($this->objeto[$this->i]['fechafinal']);
				$this->objeto[$this->i]['fechafinal']=$date->format('d-m-Y');*/
				$this->objeto[$this->i]['costo']=$this->db_resultado['costo'];

				
			}
			$this->mysqli->close();
			echo json_encode($this->objeto);

		}
		function Agregar($nombre, $genero, $duracion, $descripcion, $clasificacion, $sala, $hora, $fecha1, $fecha2, $costo){
			session_start();
			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			$this->salida="true";
			$genero=serialize($genero);
			$this->tupla = "INSERT INTO pelicula (nombre, genero, duracion, descripcion, clasificacion, idSala, hora, fechadeinicio, fechafinal, costo) VALUES ('$nombre', '$genero', '$duracion', '$descripcion', '$clasificacion', '$sala', '$hora', '$fecha1', '$fecha2', '$costo')";
			$this->resultado = $this->mysqli->query($this->tupla) or $this->salida=$this->mysqli->error;
			$_SESSION['idPelicula']=$this->mysqli->insert_id;
			if($this->salida=="true"){
				$this->salida2="true";
				$puestos=Array();
                for($i=0; $i<50; $i++){
                    $puestos[$i]=0;                       
                }
                $puestos=serialize($puestos);
                $id=$this->mysqli->insert_id;
				$this->tupla2 = "INSERT INTO sillas (idPelicula, disponible) VALUES ('$id', '$puestos')";
				$this->resultado2 = $this->mysqli->query($this->tupla2) or $this->salida2=$this->mysqli->error;

				if($this->salida2!="true"){
					$this->salida="false";
					$this->mysqli->query("DELETE FROM  pelicula WHERE  id='$id'");					

				}
			}
			
			$this->mysqli->close();
			echo json_encode($this->salida2);
		}
		function ObtenerPeliculas(){
			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			$this->tupla = "SELECT * FROM pelicula";
			$this->resultado = $this->mysqli->query($this->tupla);
			$this->objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			
			while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$this->objeto[$this->i]['id']=$this->db_resultado['id'];
				$this->objeto[$this->i]['nombre']=$this->db_resultado['nombre'];
				$this->objeto[$this->i]['genero']=unserialize($this->db_resultado['genero']);
				$this->objeto[$this->i]['duracion']=$this->db_resultado['duracion'];
				$this->objeto[$this->i]['descripcion']=$this->db_resultado['descripcion'];
				$this->objeto[$this->i]['clasificacion']=$this->db_resultado['clasificacion'];
				$this->objeto[$this->i]['idSala']=$this->db_resultado['idSala'];
				$this->objeto[$this->i]['hora']=$this->db_resultado['hora'];
				$this->objeto[$this->i]['fechadeinicio']=$this->db_resultado['fechadeinicio'];
				$this->objeto[$this->i]['fechafinal']=$this->db_resultado['fechafinal'];
				$this->objeto[$this->i]['costo']=$this->db_resultado['costo'];				
				$this->i++;
			}
			$this->mysqli->close();
			echo json_encode($this->objeto);
		}

		function ObtenerSillasDisponibles($id){
			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			$this->tupla = "SELECT sillas.disponible, pelicula.costo FROM sillas  INNER JOIN  pelicula on  sillas.idPelicula=pelicula.id WHERE  idPelicula='$id'";
			$this->resultado = $this->mysqli->query($this->tupla);
			$this->objeto[0]['m']=$this->resultado->num_rows;
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$this->objeto[0]['disponible']=unserialize($this->db_resultado['disponible']);
				$this->objeto[0]['costo']=$this->db_resultado['costo'];
			}
			$this->mysqli->close();
			echo json_encode($this->objeto);

		}

		function ComprarSilla($id, $puestos){

			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			session_start();
			$this->idUsuario=$_SESSION['id'];
			$cont=0;
			$asientos=array();
			$i=0;
			for($i=0; $i<50; $i++){
             	if($puestos[$i]==-1)
             		{
             			$puestos[$i]=1;
             			$asientos[$cont]=$i;
             			$cont++;
             		}                       
                }
			$puestos=serialize($puestos);
			$asientos=serialize($asientos);
			$date=date("Y-m-d");
			$this->tupla = "UPDATE  sillas  SET  disponible='$puestos' WHERE  idPelicula='$id'";
			$this->resultado = $this->mysqli->query($this->tupla);
			

			$this->tupla3="SELECT  costo, pelicula.nombre as pelicula, sala.nombre as sala, hora FROM  pelicula  INNER JOIN  sala on pelicula.idSala=sala.id WHERE  pelicula.id='$id'";
			$this->resultado3 = $this->mysqli->query($this->tupla3);

			if($this->db_resultado = mysqli_fetch_array($this->resultado3, MYSQLI_ASSOC))
			{
				
				$this->objeto[0]['costo']=$this->db_resultado['costo'];
				$this->objeto[0]['nombrePelicula']=$this->db_resultado['pelicula'];
				$this->objeto[0]['sala']=$this->db_resultado['sala'];
				$this->objeto[0]['hora2']=$this->db_resultado['hora'];
			}
			date_default_timezone_set('America/Caracas');
			$this->objeto[0]['Fecha']=date("d-m-Y");
			$this->objeto[0]['asientos']=unserialize($asientos);
			$this->objeto[0]['cantidad']=$cont;
			$this->objeto[0]['hora']=date('h:i:s');

			$this->objeto[0]['nombre']=$_SESSION['Nombre']. "  ".$_SESSION['Apellido'];
			$total=$this->objeto[0]['cantidad']*$this->objeto[0]['costo'];
			/*$asientos=serialize($asientos);*/
			$hora=date('h:i:s');
			$fecha=date("Y-m-d");
			$this->tupla4="INSERT INTO factura  (idUsuario, idPelicula, puestos, cantidad, fecha, hora, total) VALUES ('$this->idUsuario', '$id', '$asientos', '$cont', '$fecha', '$hora', '$total')";
			$this->resultado3 = $this->mysqli->query($this->tupla4);
			$this->objeto[0]['id']=$this->mysqli->insert_id;
			echo json_encode($this->objeto);			
		}

		function BuscarPelicula($data){
			$this->mysqli = new mysqli(Host, User, Pass, BasedeDatos);
			$this->fecha=date("Y-m-d");
			$this->tupla="SELECT pelicula.*, pelicula.nombre as nombrepelicula, pelicula.id as idPelicula, sala.*, sala.nombre as salanombre,  sala.id as idSala FROM  pelicula INNER JOIN sala on pelicula.idSala=sala.id  WHERE  (pelicula.nombre  like '%$data%' or pelicula.descripcion like '%$data%' or pelicula.genero like '%$data%' or pelicula.clasificacion like '%$data%' ) AND '$this->fecha'>=fechadeinicio and '$this->fecha'<=fechafinal ";
			$this->resultado = $this->mysqli->query($this->tupla);
			$this->objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			
			while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$this->objeto[$this->i]['idPelicula']=$this->db_resultado['idPelicula'];
				$this->objeto[$this->i]['nombre']=$this->db_resultado['nombrepelicula'];
				$this->objeto[$this->i]['genero']=unserialize($this->db_resultado['genero']);
				$this->objeto[$this->i]['duracion']=$this->db_resultado['duracion'];
				$this->objeto[$this->i]['descripcion']=$this->db_resultado['descripcion'];
				$this->objeto[$this->i]['clasificacion']=$this->db_resultado['clasificacion'];
				$this->objeto[$this->i]['idSala']=$this->db_resultado['idSala'];
				$this->objeto[$this->i]['salanombre']=$this->db_resultado['salanombre'];
				$this->objeto[$this->i]['hora']=$this->db_resultado['hora'];
				$this->objeto[$this->i]['fechadeinicio']=$this->db_resultado['fechadeinicio'];
				$this->objeto[$this->i]['fechafinal']=$this->db_resultado['fechafinal'];
				$date = new DateTime($this->objeto[$this->i]['fechafinal']);
				$this->objeto[$this->i]['fechafinal']=$date->format('d-m-Y');
				$this->objeto[$this->i]['costo']=$this->db_resultado['costo'];



				
				$this->i++;
			}
			$this->mysqli->close();
			echo json_encode($this->objeto);
		}


	}

?>