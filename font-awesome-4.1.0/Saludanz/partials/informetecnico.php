
				<div class="row">
				            <div class="col-xs-12">
				                 <ol class="breadcrumb">
				                    <li id="menuprincipal"><a href="#">MENU PRINCIPAL</a>
				                    </li>
				                    <li class="active">INFORME TECNICO</li>
				                </ol>
				            </div>
				    </div>
				    <br>
				     <div class="row">
			            <div class="col-xs-4">
			                 
			            </div>
			            <div class="col-xs-2">
			                 <button type="button" class="btn btn-default btn-xs" id="guardar"><span class="glyphicon glyphicon-floppy-save"></span> GUARDAR</button>
			            </div>
			            <div class="col-xs-2">
			                 <button type="button" class="btn btn-default btn-xs" id="modificar" disabled><span class="glyphicon glyphicon-pencil"></span> MODIFICAR</button>
			            </div>
			            <div class="col-xs-2">
			                 <button type="button" class="btn btn-default btn-xs" id="eliminar" disabled><span class="glyphicon glyphicon-remove"></span> ELIMINAR</button>
			            </div>
			            <div class="col-xs-2">
			                 <button type="button" class="btn btn-default btn-xs" id="imprimir"><span class="glyphicon glyphicon-print"></span> IMPRIMIR</button>
			            </div>
			    </div>
				    <br>
				    <hr>
				     <div class="row">
						<div class="col-xs-7">
						</div>
						<div class="col-xs-2">
							Fecha:
						</div>
						<div class="col-xs-3">
							<?php echo date('d-m-Y'); ?>
						</div>
		      		 </div>
		      		  <div class="row">
							<div class="col-xs-7">
							</div>
							<div class="col-xs-2">
								Reloj:
							</div>
							<div class="col-xs-3" id="reloj2" name="reloj2">
								
							</div>
		      		 </div>
		      		 <div id="alertas"></div>
		      		 <hr>
		      		 <br>
				    <div class="row" id="divbuscar">
				           <div class="col-xs-3">							
							Nº DE SOLICITUD					
						</div>
						  <div class="col-xs-3">
								<input type="text" class="form-control input-sm" id="tnumero">
						</div>
						  <div class="col-xs-1">
							<button class="btn btn-default btn-xs" type="button" id="buscar"><span class="glyphicon glyphicon-search"></span>Buscar</button>
						</div>			
				            
				    </div>
				    <br>
				    <hr>
				    <!-- <div class="alinearizquierda2">Solicitudes Procesadas</div>
				    <hr> -->
				    <div class="alinearizquierda">Datos de solicitud</div>
					    <div class="datagrid" id="listado">
	   						 <table style="width:100%;" class="table hover">
	   						 <thead>
	   						 	<tr>
	   						 		<th>
				        				Nº DE SOLICITUD
				        			</th>
				        			<th>
				        				FECHA
				        			</th>
				        			<th>
				        				HORA
				        			</th>
				        			<th>
				        				REPORTADA POR
				        			</th>
				        			<th>
				        				ESTATUS
				        			</th>
	   						 	</tr>
	   						 </thead>
	   						 <tbody>
	   						 	<tr>
	   						 		<td></td>
	   						 		<td></td>
	   						 		<td></td>
	   						 		<td></td>
	   						 		<td></td>
	   						 	</tr>
	   						 </tbody>
	   						</table>				  
					    </div>
				    <br>
				    <div class="alinearizquierda">Datos de Ubicacion</div>
				       <div class="datagrid" id="listado2" >
	   						 <table style="width:100%;" class="table hover">
	   						 <thead>
	   						 	<tr>
	   						 		<th>
				        				UNIDAD GENERAL
				        			</th>
				        			<th>
				        				SUB-UNIDAD GENERAL
				        			</th>
				        			<th>
				        				UNIDAD
				        			</th>
				        			<th>
				        				SUB-UNIDAD
				        			</th>
				        			<th>
				        				TELEFONO
				        			</th>
	   						 	</tr>
	   						 </thead>
	   						 <tbody>
	   						 	<tr>
	   						 		<td></td>
	   						 		<td></td>
	   						 		<td></td>
	   						 		<td></td>
	   						 		<td></td>
	   						 	</tr>
	   						 </tbody>
	   						</table>				  
					    </div>
				    <br>
				    <hr>
				    <br>
				     <div class="row">
				            <div class="col-xs-2">
				                 Tipo de Falla: 
				            </div>
				             <div class="col-xs-4">
				                 <select class="form-control input-sm" id="tipo">
				                 	 <option value="-1"></option>
		                                <option >FALLA 1 </option>
		                                <option >FALLA 2</option>
		                                <option >FALLA 3</option>
				                 </select>
				            </div>
				            <div class="col-xs-1">
				                 Descripcion: 
				            </div>
				            <div class="col-xs-5">
				                 <textarea rows="2" cols="2" class="form-control input-sm" id="descripcion" disabled></textarea>
				            </div>         
				           
				    </div>
				    <div class="alinearizquierda">Detalles del servicio</div>
				    <div class="row">
				            <div class="col-xs-2">
				                 Diagnostico:
				            </div>
				            <div class="col-xs-10">
				            	<textarea rows="2" cols="2" class="form-control input-sm" id="diagnostico"></textarea>
				            </div>				            
				    </div>
				    <BR>
				     <div class="row">
				            <div class="col-xs-2">
				                Solucion:
				            </div>
				            <div class="col-xs-1">
				                
				            </div>
				            <div class="col-xs-10">
				                 <textarea rows="2" cols="2" class="form-control input-sm" id="solucion"></textarea>
				            </div>				            
				    </div>
				    <BR>
				    <div class="row">
				            <div class="col-xs-2">
				                Observacion:
				            </div>
				            <div class="col-xs-10">
				                 <textarea rows="2" cols="2" class="form-control input-sm" id="observacion"></textarea>
				            </div>				            
				    </div>
				    <BR>
				    <div class="row">
				            <div class="col-xs-2">
				                Estatus:
				            </div>
				            
				            <div class="col-xs-10">
				                 <select class="form-control input-sm" id="estado">
				                 	<option value="-1"></option>
				                 	<option value="3">Solucionado</option>
				                 	<option value="4">No Solucionado</option>
				                 </select>
				            </div>				            
				    </div>
				    <BR>
				    <hr>
				    <div class="alinearizquierda">Tipo de soporte realizado</div>
				    <BR>
				    <div class="alinearizquierda2">
				    	
				        <input type="radio" id="soporte" checked="true">
				      <label>Soporte en Sitio</label><br>
				   
				        <input type="radio" id="viaTelefonica" >
				      <label>Via telefonica</label><br>
				     <input type="radio" id="traslado">
				      <label>Traslado a Trailer de Soporte</label><br>
				    </div>


			</div>     
      </div>

    
<script type="text/javascript">	
	$(document).ready(function() {
		validarRadios();
		var soporte2=1;
		var numero="";
		$('#imprimir').on("click", function() {
			//$('#divbuscar').fadeOut(function{
				
			focus();
        	print();
			//});
		});
		$('#modificar').on("click", function() {

				$.ajax
					({
					type: "POST",
				   	url: "modelo/consultasAsignaciones.php",
				   	data: {id:4, diagnostico:diagnostico.value, solucion:solucion.value, observacion:observacion.value, estado:estado.value, soporte:soporte2, numero:numero},
					async: false,	
					dataType: "json",	
					success:
				    function (msg) 
					{	
						console.log("Salida: "+msg);	
						$('#modificar').attr('disabled', true);
						$('#eliminar').attr('disabled',true);
						$('#imprimir').attr('disabled',true);
						$('#guardar').attr('disabled',false);		
						limpiar();			
						
				     },
				        error:
				        function (msg) {console.log( msg +"No se pudo realizar la conexion");}
					});
		});
		$('#guardar').on("click", function() {
			//console.log("Reloj: "+ );
			if(validarRegistro()){
				$.ajax
					({
					type: "POST",
				   	url: "modelo/consultasAsignaciones.php",
				   	data: {id:1, diagnostico:diagnostico.value, solucion:solucion.value, observacion:observacion.value, estado:estado.value, soporte:soporte2, numero:numero, reloj:reloj(2)},
					async: false,	
					dataType: "json",	
					success:
				    function (msg) 
					{	
						console.log("Salida: "+msg);	
						/*$('#modificar').attr('disabled',false);
						$('#eliminar').attr('disabled',false);
						$('#imprimir').attr('disabled',false);
						$('#guardar').attr('disabled',true);	*/	
						limpiar();			
						
				     },
				        error:
				        function (msg) {console.log( msg +"No se pudo realizar la conexion");}
					});
			}
		});
		$('#eliminar').on("click", function() {
			$.ajax
					({
					type: "POST",
				   	url: "modelo/consultasAsignaciones.php",
				   	data: {id:3,numero:numero},
					async: false,	
					dataType: "json",	
					success:
				    function (msg) 
					{	
						alertas.innerHTML='<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Informe Tecnico eliminado correctamente!</div>';
						limpiar();

				     },
				        error:
				        function (msg) {console.log( msg +"No se pudo realizar la conexion");}
					});
		});
		$('#buscar').on("click", function() {
			if (tnumero.value!="") {
				console.log("Numero a buscar "+tnumero.value);
				$.ajax
					({
					type: "POST",
				   	url: "modelo/consultasServicio.php",
				   	data: {id:6, idservicio:tnumero.value},
					async: false,	
					dataType: "json",	
					success:
				    function (msg) 
					{	console.log(msg[0].m);
						if(msg[0].m>0){
							numero=tnumero.value;
							 $('#listado').html("");
	                        
	                        var table = $('<table style="width:100%;" class="table"></table>');                         
	                        var row=$('<thead></thead>').append("<tr></tr>")
	                        $('#listado').append(ListarTabla(msg, table, row)); 

	                         $('#listado2').html("");
	                        
	                        var table = $('<table style="width:100%;" class="table"></table>');                         
	                        var row=$('<thead></thead>').append("<tr></tr>")
	                        $('#listado2').append(ListarTabla2(msg, table, row)); 
	                        $('#tipo').val(msg[0].tipodefalla);
	                        $('#descripcion').val(msg[0].descripciondefalla);

	                            
						}
						else{
							alertas.innerHTML='<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Solicitud de Servicio no encontrado!</div>';
								$('#listado').html("");
	                        
		                        var table = $('<table style="width:100%;" class="table"></table>');                         
		                        var row=$('<thead></thead>').append("<tr></tr>")
		                        $('#listado').append(ListarTabla(msg, table, row)); 

		                         $('#listado2').html("");
		                        
		                        var table = $('<table style="width:100%;" class="table"></table>');                         
		                        var row=$('<thead></thead>').append("<tr></tr>")
		                        $('#listado2').append(ListarTabla2(msg, table, row)); 
		                        $('#tipo').val(-1);
		                        $('#descripcion').val("");
						}
						
				     },
				        error:
				        function (msg) {console.log( msg +"No se pudo realizar la conexion");}
					});

				$.ajax
					({
					type: "POST",
				   	url: "modelo/consultasAsignaciones.php",
				   	data: {id:2,numero:numero},
					async: false,	
					dataType: "json",	
					success:
				    function (msg) 
					{	
						if(msg[0].m>0){
							numero=msg[0].numero;
							diagnostico.value=msg[0].diagnostico;
							solucion.value=msg[0].solucion;
							observacion.value=msg[0].observacion;
							$('#estado').val(msg[0].estado);
							if(msg[0].soporte==1){
							soporte.checked=true;
							viaTelefonica.checked=false;
							traslado.checked=false;
							}	
							if(msg[0].soporte==2){
							viaTelefonica.checked=true;
							soporte.checked=false;
							traslado.checked=false;
							}		
							if(msg[0].soporte==3){
							traslado.checked=true;
							soporte.checked=false;
							viaTelefonica.checked=false;
							}
							soporte2=msg[0].soporte;
							$('#modificar').attr('disabled',false);
						$('#eliminar').attr('disabled',false);
						$('#imprimir').attr('disabled',false);
						$('#guardar').attr('disabled',true);
						}

				     },
				        error:
				        function (msg) {console.log( msg +"No se pudo realizar la conexion");}
					});


			}
		});
	 function limpiar(){
					$('#listado').html("");

					var table = $('<table style="width:100%;" class="table"></table>');                         
					var row=$('<thead></thead>').append("<tr></tr>");
					row.append($('<th></th>').html(""));
					row.append($('<th></th>').html("<b>Nº DE SOLICITUD</b>"));                            
					row.append($('<th></th>').html("<b>FECHA</b>"));
					row.append($('<th></th>').html("<b>HORA</b>"));
					row.append($('<th></th>').html("<b>REPORTADO POR</b>"));
					row.append($('<th></th>').html("<b>ESTATUS</b>"));
					table.append(row);  
					var row2 = $('<tbody></tbody>');
					var row3=$('<tr></tr>');
					row2.append(row3); 
					table.append(row2);
					$('#listado').append(table);

					$('#listado2').html("");
                    
                    var table = $('<table style="width:100%;" class="table"></table>');                         
                    var row=$('<thead></thead>').append("<tr></tr>");
                     var table = $('<table style="width:100%;" class="table"></table>');                         
                    var row=$('<thead></thead>').append("<tr></tr>")
                    row.append($('<th></th>').html("<b>UNIDAD GENERAL</b>"));                            
	                row.append($('<th></th>').html("<b>SUB-UNIDAD GENERAL</b>"));
	                row.append($('<th></th>').html("<b>UNIDAD</b>"));
	                row.append($('<th></th>').html("<b>SUB-UNIDAD</b>"));
	                row.append($('<th></th>').html("<b>TELEFONO</b>"));
	                table.append(row);  

	                var row2 = $('<tbody></tbody>');
	                var row3=$('<tr></tr>');
	                row2.append(row3); 
                    table.append(row2);
                    $('#listado2').append(table); 
                    $('#tipo').val(-1);
                    $('#descripcion').val("");
                    diagnostico.value="";
                    solucion.value="";
                    observacion.value="";
                    estado.value="";
                    soporte2=1;
                    soporte.checked=true;
                    $('#estado').val(-1);
	 }
	function validarRadios(){        
			$('#soporte').on('click',  function(){
				if ($('input[id="soporte"]').is(':checked'))
				{	console.log("Radio Soporte Activado");
					viaTelefonica.checked=false;
					traslado.checked=false;		
					soporte2=1;	
				}				
			});
			$('#viaTelefonica').on('click',  function(){
				if ($('input[id="viaTelefonica"]').is(':checked'))				
				{	console.log("radio viaTelefonica Activado");
					soporte.checked=false;
					traslado.checked=false;				
					soporte2=2;	
				}				
			});
			$('#traslado').on('click',  function(){
				if ($('input[id="traslado"]').is(':checked'))
				{
				    console.log("traslado Activado");
					soporte.checked=false;
					viaTelefonica.checked=false;
					soporte2=3;
				}				
			});
	}
	function ListarTabla2(msg, table, row){         
                row.append($('<th></th>').html("<b>UNIDAD GENERAL</b>"));                            
                row.append($('<th></th>').html("<b>SUB-UNIDAD GENERAL</b>"));
                row.append($('<th></th>').html("<b>UNIDAD</b>"));
                row.append($('<th></th>').html("<b>SUB-UNIDAD</b>"));
                row.append($('<th></th>').html("<b>TELEFONO</b>"));
                table.append(row);  
                var row2 = $('<tbody></tbody>');

                for(i=0; i<msg[0].m; i++){
                   
                        var row3=$('<tr></tr>');
                        
                        var fila1 = $('<td></td>').text(msg[i].unidadgeneral);
                        var fila2 = $('<td></td>').text(msg[i].subunidadgeneral);
                        var fila3 = $('<td></td>').text(msg[i].unidad);
                        var fila4 =$('<td></td>').text(msg[i].unidadgeneral2);   
                        var fila5 =$('<td></td>').text(msg[i].telefono);   

                        row3.append(fila1);
                        row3.append(fila2);
                        row3.append(fila3);
                        row3.append(fila4); 
                        row3.append(fila5);  

                        row2.append(row3);                              
                       
                    
                     
                }
                table.append(row2);
                return table;
            }
 		function ListarTabla(msg, table, row){         
                row.append($('<th></th>').html(""));
                row.append($('<th></th>').html("<b>Nº DE SOLICITUD</b>"));                            
                row.append($('<th></th>').html("<b>FECHA</b>"));
                row.append($('<th></th>').html("<b>HORA</b>"));
                row.append($('<th></th>').html("<b>REPORTADO POR</b>"));
                row.append($('<th></th>').html("<b>ESTATUS</b>"));
                table.append(row);  
                var row2 = $('<tbody></tbody>');

                for(i=0; i<msg[0].m; i++){
                   
                        var row3=$('<tr></tr>');
                        var fila0=$('<td></td>').text((i+1));
                        var fila1 = $('<td></td>').text(msg[i].id);
                        console.log("Id: " +msg[i].id);
                        var fila2 = $('<td></td>').text(msg[i].fechadeingreso);
                        var fila3 = $('<td></td>').text(msg[i].hora);
                        var fila4 =$('<td></td>').text(msg[i].reportadopor);
                        var fila5="";
                        if(msg[i].estatus=="1")
                        var fila5 = $('<td></td>').text("No Asignado");
                        if(msg[i].estatus=="2")
                        var fila5 = $('<td></td>').text("Asignado");
                        if(msg[i].estatus=="3")
                        var fila5 = $('<td></td>').text("Solucionado");
                        if(msg[i].estatus=="4")
                        var fila5 = $('<td></td>').text("No Solucionado");                             
                        row3.append(fila0);
                        row3.append(fila1);
                        row3.append(fila2);
                        row3.append(fila3);
                        row3.append(fila4);  
                        row3.append(fila5);
                        row2.append(row3);                              
                       
                    
                     
                }
                table.append(row2);
                return table;
            }
        function validarRegistro(){
			if(diagnostico.value=="")
			{  
		        alertas.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>Diagnostico</b>!</div>';
				return false;
			}		
			
			else if(solucion.value=="")
			{  
				alertas.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>Solucion</b>!</div>';
				return false;
			}
			else if(observacion.value=="")
			{  
				alertas.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>Observacion</b>!</div>';
				return false;
			}			
			else if(estado.value=="-1")
			{  
		        alertas.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor Seleccione un <b>Estatus</b>!</div>';
				return false;
			}
			
			else {return true;}
	    }


	});
</script>


