<div class="panel panel panel-info">
  <div class="panel-heading">Comprar</div>
  <div class="panel-body" >
    
   <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#tabComprar" aria-controls="home" role="tab" data-toggle="tab">Comprar</a></li>
     <!--  <li role="presentation"><a href="#tabBuscar" aria-controls="profile" role="tab" data-toggle="tab">Buscar</a></li>
      <li role="presentation" id="listado"><a href="#tabListado" aria-controls="profile" role="tab" data-toggle="tab">Listado</a></li> -->
    </ul>

     <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="tabComprar"> 
              <hr>  
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Numero, genero, clasificacion" id="tbuscarPelicula">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="buscarPelicula" >Buscar Pelicula!</button>
                  </span>
                </div>
                <hr>
                <div id="Listadospeliculas">
                  
                </div>
                 
                 
            </div>
            <!-- <div role="tabpanel" class="tab-pane" id="tabBuscar">
                  <hr>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Numero, Nombre" id="tbuscarSala">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button" id="buscarSala" >Buscar Sala!</button>
                    </span>
                  </div>
                  <div id="alertaBuscarSala" style="display:none;">
                          <div class="alert alert-warning" role="alert">
                               <a href="#" class="alert-link"></a>
                          </div>
                  </div>
                  <hr>
                  <div id="resultados">
                    
   
            </div> -->
     </div>


 
  </div>
</div>


<div class="modal fade" id="ModalComprarPelicula" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
        <h4 class="modal-title" id="myModalLabel">Comprar Pelicula</h4>
      </div>
      <div class="modal-body" id="contenedorPuestos">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="comprarEntradas" style="display:none;">Comprar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ModalFacturaPelicula" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
      </div>
      <div class="modal-body" id="Factura">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript"> 
    $(document).ready(function() {
     BuscarPelicula();
      function Comprar(Puestos, ID){
        $('#comprarEntradas').on('click',  function(){
       /*   console.log("click en comprar entradas");*/
            $.ajax
                  ({
                  type: "POST",
                  url: "modelo/ControladorPelicula.php",
                  data: {id:4, ID:ID, puestos:Puestos},
                  dataType: "json",
                  async: false,           
                  success:
                  function (msg) 
                  {   
                      /*console.log(msg);*/
                      $('#ModalComprarPelicula').modal('hide');
                      $('#ModalFacturaPelicula').modal('show');
                      $("#Factura").load('partials/factura.php', function(){
                        fecha22.innerHTML=msg[0].Fecha;
                        hora.innerHTML=msg[0].hora;
                        ticket.innerHTML=msg[0].id;

                        total2.innerHTML=(msg[0].cantidad*parseFloat(msg[0].costo))+" Bs";
                        customertitle.innerHTML=msg[0].nombre;
                         var fila1 = $('<td class="item-name"></td>').html(msg[0].nombrePelicula);
                         var asientos="";  
                         for (var i =0; i<(msg[0]['asientos']).length; i++) {
                            if(i==0)
                            asientos=msg[0]['asientos'][i];
                            if(i>0)
                              asientos=asientos+ ", "+msg[0]['asientos'][i];

                          }
                         var fila2 = $('<td></td>').html(asientos);         
                         var fila3 = $('<td></td>').html(msg[0].sala);
                          var fila4 ="";
                          if(msg[0].hora2=="1"){
                              fila4 = $('<td></td>').text("6:30PM-9:00PM");               
                            }
                             if(msg[0].hora2=="2"){
                              fila4 = $('<td></td>').text("4:30PM-6:30PM");               
                            }
                             if(msg[0].hora2=="3"){
                              fila4 = $('<td></td>').text("2:30-PM-4:30PM");                
                            } 
                         var fila5 = $('<td></td>').html(msg[0].costo);  
                         var fila6 = $('<td></td>').html(msg[0].cantidad);
                         var fila7 = $('<td></td>').html((msg[0].cantidad*parseFloat(msg[0].costo)));
                       
                        $('#detallesdecompra').append(fila1);
                        $('#detallesdecompra').append(fila2);
                        $('#detallesdecompra').append(fila3);
                        $('#detallesdecompra').append(fila4);
                        $('#detallesdecompra').append(fila5);
                        $('#detallesdecompra').append(fila6);
                        $('#detallesdecompra').append(fila7);




                      });

                  },
                      error:
                      function (msg) {console.log( msg +"No se pudo realizar la conexion");}
                  });
        });
      }

      function BuscarPelicula(){
       $('#buscarPelicula').on('click',  function(){
          console.log("click en Buscar Pelicula " + tbuscarPelicula.value );

          if(tbuscarPelicula.value!=""){
            $.ajax
                  ({
                  type: "POST",
                  url: "modelo/ControladorPelicula.php",
                  data: {id:5, data:tbuscarPelicula.value},
                  dataType: "json",
                  async: false,           
                  success:
                  function (msg) 
                  {   
                      if(msg[0].m>0){
                      var comprados=0;
                      $('#Listadospeliculas').html("");
                      var table = $('<table></table>').addClass('table table-hover');             
                      var row=$('<tr></tr>');
                      $('#Listadospeliculas').append(ListarTabladeCartelera(msg, table, row));
                      
                      eventosComprarPelicula(comprados);}
                      else {
                        $('#Listadospeliculas').html("<div class='alert alert-danger'>¡No se ha encontrado la pelicula!</div>");
                      }
                  },
                      error:
                      function (msg) {console.log( msg +"No se pudo realizar la conexion");}
                  });
          }
          else
             $('#Listadospeliculas').html("<div class='alert alert-danger'>¡Escriba el nombre de la pelicula!</div>");
        }); 
      }
      function eventosComprarPelicula(comprados){
        $('a.ComprarPelicula').on('click',  function(){
            /*console.log("click en comprar2:");*/
             var idPelicula=$(this).attr('name');
             var costo=0;
             $("#contenedorPuestos").hide().load('partials/silla.html', function(){
                puestos=new Array();
                $.ajax
                  ({
                  type: "POST",
                  url: "modelo/ControladorPelicula.php",
                  data: {id:3, ID:idPelicula},
                  dataType: "json",
                  async: false,           
                  success:
                  function (msg) 
                  {   
                      /*console.log("sillas compradas: "+comprados);*/
                      comprados=0;
                       puestos=msg[0].disponible;
                       costo=msg[0].costo;
                   },
                      error:
                      function (msg) {console.log( msg +"No se pudo realizar la conexion");}
                  });

                $('#listadodeasientos').html("");                    
                var table = $('<table></table>').addClass('table table-hover');           
                var row=$('<tr></tr>');
                $('#listadodeasientos').append(listadodeasientos(table, row, puestos));
                

                $('a.puesto').on('click',  function(){
                  
                   var id=$(this).attr('id');
                  
                  console.log("id de la silla: "+id+ " posicion vacio? "+puestos[id]);
                  if(puestos[id]==0){
                    puestos[id]=-1;
                    comprados++;
                    if(comprados>0)
                      $('#comprarEntradas').fadeIn();
                    else
                       $('#comprarEntradas').fadeOut();
                    (document.getElementById("costo")).innerHTML="Sillas Compradas: " +comprados +"  Costo "+(parseFloat(costo)*comprados)+" Bs";
                   /* console.log("Costo "+(costo*comprados)+" Bs");
                    console.log(" Cantidad Comprada: "+comprados + " Sillas: "+puestos);*/
                    var id2=id;
                    if(id2>=10&&id2<20)
                      id2=id2%10;
                    if(id2>=20&&id2<30)
                      id2=id2%20;
                    if(id2>=30&&id2<40)
                      id2=id2%30;
                    if(id2>=40&&id2<50)
                      id2=id2%40;

                    id2;
                    
                    (document.getElementById(id)).innerHTML='<img src="img/cine4.png" class="silla"><br><center>'+(id)+'</center>';

                    return
                  }
                  if(puestos[id]==-1){
                    puestos[id]=0;
                    comprados--;
                    if(comprados>0)
                      $('#comprarEntradas').fadeIn();
                    else
                       $('#comprarEntradas').fadeOut();
                    (document.getElementById("costo")).innerHTML="Sillas Compradas: " +comprados +"  Costo "+(parseFloat(costo)*comprados)+" Bs";

                    /*  console.log("Costo "+(costo*comprados)+" Bs");
                    console.log(" Cantidad Comprada: "+comprados + " Sillas: "+puestos);*/
                    (document.getElementById(id)).innerHTML='<img src="img/silla.png" class="silla"><br><center>'+(id)+'</center>';
                    return
                  }

                  
                });
                Comprar(puestos, idPelicula);
                
             }).slideToggle(1000);

        });
      }
            $.ajax
              ({
              type: "POST",
              url: "modelo/ControladorPelicula.php",
              data: {id:1},
              dataType: "json",
              async: false,           
              success:
              function (msg) 
              {   
                   var comprados=0;
                    $('#Listadospeliculas').html("");
                    var table = $('<table></table>').addClass('table table-hover');             
                    var row=$('<tr></tr>');
                    $('#Listadospeliculas').append(ListarTabladeCartelera(msg, table, row));
                    

                    eventosComprarPelicula(comprados);
               },
                  error:
                  function (msg) {console.log( msg +"No se pudo realizar la conexion");}
              });


              function ListarTabladeCartelera(msg, table, row){     
               /* console.log("listado de peliculas: "+msg[0].m );*/
                table.append(row);          
                if(msg[0].m<5){
                    for(i=0; i<msg[0].m; i++){
                      var row2 = $('<tr></tr>');
                    //  var fila1=$('<td></td>').html('<img src="http://200.74.197.179/imagenes/cartelera/EL%20PERRO%20MILLONARIO.jpg" class="img-responsive" alt="Responsive image">');
                      var fila2 = $('<td></td>').html("<b>"+msg[i].nombre+"</b><br><b>Genero:</b> "+msg[i].genero+"<br><b>Duracion:</b> "+msg[i].duracion+ " minutos");      
                     // row2.append(fila1);
                      var fila3 = $('<td></td>').append('<a  class="btn btn-default btn-sm ComprarPelicula" data-toggle="modal" data-target="#ModalComprarPelicula"  title="ComprarPelicula" name="'+msg[i].id+'"> <span class="glyphicon glyphicon-star">Comprar Pelicula</span></a>');
                      row2.append(fila2); 
                      row2.append(fila3);                     
                      table.append(row2);
                    }
                }
                else{
                  for(i=0; i<5; i++){
                      var row2 = $('<tr></tr>');
                     // var fila1=$('<td></td>').html('<img src="http://200.74.197.179/imagenes/cartelera/EL%20PERRO%20MILLONARIO.jpg" class="img-responsive" alt="Responsive image">');
                      var fila2 = $('<td></td>').html("<b>"+msg[i].nombre+"</b><br><b>Genero:</b> "+msg[i].genero+"<br><b>Duracion:</b> "+msg[i].duracion+ " minutos");         
                     // row2.append(fila1);
                     var fila3 = $('<td></td>').append('<a  class="btn btn-default btn-sm ComprarPelicula" data-toggle="modal" data-target="#ModalComprarPelicula"  title="ComprarPelicula" name="'+msg[i].id+'"> <span class="glyphicon glyphicon-star">Comprar Pelicula</span></a>');
                      row2.append(fila2);  
                      row2.append(fila3);                    
                      table.append(row2);
                    } 
                }

                return table;
          }

          function listadodeasientos( table, row, puesto){        
            var cont=0;
              for(i=0; i<5; i++){
                var row2 = $('<tr></tr>');
                if(i==0)
                  row2.append($('<td width="50" ></td>').html('E'));
                if(i==1)
                  row2.append($('<td width="50" ></td>').html('D'));
                if(i==2)
                  row2.append($('<td width="50" ></td>').html('C'));
                if(i==3)
                  row2.append($('<td width="50" ></td>').html('B'));
                if(i==4)
                  row2.append($('<td width="50" ></td>').html('A'));
                for (var j = 0; j<11; j++) {

                  if(j==5)
                    row2.append($('<td width="50" ></td>').html(''));
                  if(j<5){
                    cont++;
                    if(puesto[cont-1]==0)
                      row2.append($('<td></td>').html('<a href="#" id="'+(cont-1)+'" class="puesto" ><img src="img/silla.png" class="silla"><br><center>'+(cont-1)+'</center> </a>')); 
                    if(puesto[cont-1]==1)
                      row2.append($('<td></td>').html('<a href="#" id="'+(cont-1)+'" class="puesto" disabled><img src="img/cine2.png" class="silla" disabled><br><center>'+(cont-1)+'</center> </a>')); 
                  }
                  if(j>5){
                    cont++;
                   if(puesto[cont-1]==0)
                    row2.append($('<td></td>').html('<a href="#" id="'+(cont-1)+'" class="puesto" ><img src="img/silla.png" class="silla"><br><center>'+(cont-1)+'</center> </a>'));  
                   if(puesto[cont-1]==1)
                       row2.append($('<td></td>').html('<a href="#" id="'+(cont-1)+'" class="puesto" disabled><img src="img/cine2.png" class="silla"><br><center>'+(cont-1)+'</center> </a>'));  
                  
                  }    
                }
                        
                table.append(row2);
              }
            return table;
          }

    });
</script>