
<div class="panel panel panel-info">
  <div class="panel-heading">Cuenta</div>
  <div class="panel-body" style="background-color: rgba(227, 227, 227, 0.7);">
    
  <?php  
      session_start(); 
      
      if(isset($_SESSION['privilegio'])&&$_SESSION['privilegio']=="1"){ 
       // echo $_SESSION['privilegio'];
        ?>
      
   <div class="row">
            <div class="col-md-3 col-xs-3 col-sm-3">
                <ul class="nav nav-pills nav-stacked" role="tablist" style="max-width: 300px;">
                  <li role="presentation" class="active" id="agregarSala"><a href="#" ><span class="glyphicon glyphicon-plus"></span> Agregar Sala</a></li>
                  <li role="presentation" id="agregarPelicula"><a href="#" ><span class="glyphicon glyphicon-plus"></span> Agregar Pelicula</a></li>
                </ul>

           </div> 
           <div class="col-md-9 col-xs-9 col-sm-9" id="contenedorCuenta">
           </div>
    </div>

    <?php
     }else {

     ?>

      <div class="row">
                  <div class="col-md-3 col-xs-3 col-sm-3">
                      <ul class="nav nav-pills nav-stacked" role="tablist" style="max-width: 300px;">
                        <li role="presentation" class="active" id="Comprar"><a href="#" >Comprar</a></li>
                        <li role="presentation" id="Registros"><a href="#" >Mis Facturas</li>
                       </ul>

                 </div> 
                 <div class="col-md-9 col-xs-9 col-sm-9" id="contenedorCuenta">
                 </div>
          </div>
     <?php

      }
     ?>

  </div>
</div>


<script type="text/javascript"> 
    $(document).ready(function() {
            
             /* $('#agregarSala').attr('class', 'active');

                $("#contenedorCuenta").hide().load('partials/sala.html', function(){}).slideToggle(1000);*/

           $('#agregarSala').on('click',  function(){
                console.log("click en agregar sala");
                $('#agregarPelicula').removeAttr('class', 'active');
                $('#agregarSala').attr('class', 'active');

                $("#contenedorCuenta").hide().load('partials/sala.html', function(){}).slideToggle(1000);
            });
           $('#agregarPelicula').on('click',  function(){
                $('#agregarSala').removeAttr('class', 'active');
                $('#agregarPelicula').attr('class', 'active');
               $("#contenedorCuenta").hide().load('partials/pelicula.html', function(){}).slideToggle(1000); 
            });  
            $('#Comprar').on('click',  function(){
                console.log("click en comprar");
                $('#Comprar').attr('class', 'active');
                $('#Registros').removeAttr('class', 'active');

               $("#contenedorCuenta").hide().load('partials/comprar.php', function(){}).slideToggle(1000); 
            });
             $('#Registros').on('click',  function(){
                console.log("click en  ventana registros");
                $('#Registros').attr('class', 'active');
                $('#Comprar').removeAttr('class', 'active');

               $("#contenedorCuenta").hide().load('partials/registro.html', function(){}).slideToggle(1000); 
            });     


        });
</script>

