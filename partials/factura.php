<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<style type="text/css">
		/*
	 CSS-Tricks Example
	 by Chris Coyier
	 http://css-tricks.com
*/

* { margin: 0; padding: 0; }
.body { font: 14px/1.4 Georgia, serif; }
#page-wrap { width: 100%; margin: 0 auto; }

textarea { border: 0; font: 14px Georgia, Serif; overflow: hidden; resize: none; }
.tablefactura { border-collapse: collapse; }
.tablefactura td, table th { border: 1px solid black; padding: 5px; }

#header { height: 35px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

#address { width: 250px; height: 150px; float: left; }
#customer { overflow: hidden; }

#logo { text-align: right; float: right; position: relative; margin-top: 25px; border: 1px solid #fff; max-width: 540px; max-height: 100px; overflow: hidden; }

#logoctr { display: none; }
#logohelp { text-align: left; display: none; font-style: italic; padding: 10px 5px;}
#logohelp input { margin-bottom: 5px; }
#customertitle { font-size: 20px; font-weight: bold; float: left; }

#meta { margin-top: 1px; width: 300px; float: right; margin-bottom: 10px; }
#meta td { text-align: right;  }
#meta td.meta-head { text-align: left; background: #eee; }
#meta td textarea { width: 100%; height: 20px; text-align: right; }

#items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
#items th { background: #eee; }
#items textarea { width: 80px; height: 50px; }
#items tr.item-row td { border: 0; vertical-align: top; }
#items td.description { width: 300px; }
#items td.item-name { width: 175px; }
#items td.description textarea, #items td.item-name textarea { width: 100%; }
#items td.total-line { border-right: 0; text-align: right; }
#items td.total-value { border-left: 0; padding: 10px; }
#items td.total-value textarea { height: 20px; background: none; }
#items td.balance { background: #eee; }
#items td.blank { border: 0; }

#terms { text-align: center; margin: 20px 0 0 0; }
#terms h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
#terms textarea { width: 100%; text-align: center;}


	</style>

</head>

<body class="body">

	<div id="page-wrap">

		<textarea id="header">Factura</textarea>
		
		<div id="identity">		
            <div id="address"><h3>Sistema de Venta de entradas al cine</h3></div>

            <div id="logo">            
              <img id="image" src="img/logo2.png" alt="logo" width="100" height="100" />
            </div>
		
		</div>
		
		<div style="clear:both"></div>
		
	 <div id="customertitle">Widget Corp.
c/o Steve Widget</div>

            <table id="meta" class="tablefactura">
                <tr>
                    <td class="meta-head">Factura #</td>
                    <td><textarea id="ticket">000123</textarea></td>
                </tr>
                <tr>

                    <td class="meta-head">Fecha</td>
                    <td><div id="fecha22">December 15, 2009</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Hora</td>
                    <td><div class="due" id="hora">$875.00</div></td>
                </tr>

            </table>
		<br>

		
		
		<table id="items" class="tablefactura">
		
		  <tr>
		      <th>Pelicula</th>
		      <th>Puesto</th>
		      <th>Sala</th>
		      <th>Hora</th>
		      <th>Costo Unitario</th>
		      <th>Cantidad</th>
		      <th>Costo</th>
		  </tr>
		  
		  
		  	
		  <tr class="item-row" id="detallesdecompra">
		      <!-- <td class="item-name"><textarea>Web Updates</textarea><a class="delete" href="javascript:;" title="Remove row">X</a></td>
		      <td ><textarea>Monthly web updates for http://widgetcorp.com (Nov. 1 - Nov. 30, 2009)</textarea></td>
		      <td><textarea class="cost">$650.00</textarea></td>
		      <td><textarea class="qty">1</textarea></td>
		      <td><span class="price">$650.00</span></td> -->
		  </tr>
		  
		 
		 
		  
		  <tr id="hiderow">
		    <td colspan="5"></td>
		  </tr>
		  
		 
		  
		  <div id="detallesCosto">
		  <tr>
		      <td colspan="3" class="blank"> </td>
		      <td colspan="3" class="total-line balance">Total</td>
		      <td class="total-value balance"><div class="due" id="total2">$875.00</div></td>
		  </tr>
		  </div>
		
		</table>
		
		
	
	</div>
	
</body>

</html>

<?php


?>