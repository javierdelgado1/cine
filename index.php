<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Sistema de venta de entradas para el Cine</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrapValidator.css" rel="stylesheet">

      <link rel="stylesheet" type="text/css" href="css/bootstrap-multiselect.css">
      
    <style type="text/css">
        body{
          background-color: #2461AA;
          background-image: url(img/bg.jpg);
          background-position: top;
          background-attachment: fixed;
          overflow-x: hidden;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>
        <div class="container">
            <nav class="navbar navbar-default" role="navigation">
              <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <img src="img/logo2.png" style="width:50px; height:50px; " >
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php" id="inicio">Inicio</a></li>
                    <li><a href="#" id="proximos">Proximos estrenos</a></li>
                    <li><a href="#" id="cartelera">Cartelera</a></li>
                   
                  </ul>
                  <form class="navbar-form navbar-left formbuscar" role="search">
                    <div class="input-group"   >
                     <input type="text" class="form-control" id="tBuscar" name="tbuscar" placeholder="Busqueda de Peliculas">
                        <span class="input-group-btn">
                          <button class="btn btn-primary" type="button" id="buscar" ><span class="glyphicon glyphicon-search"></span>Buscar </button>
                        </span>           
                    </div>
                  </form>
                  <ul class="nav navbar-nav navbar-right">
                    
                    

                    <?php
                        session_start(); 
                        if(isset($_SESSION['Nombre'])){
                            
                        echo "<li> <a href='#' > ".$_SESSION['Nombre']."</a></li>"

                    ?>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mi Perfil<span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                        <li id="cuenta"><a href="#" >Cuenta</a></li>
                        <li id="configurar"><a href="#"  >Configurar</a></li>
                        
                        <li class="divider"></li>
                        <li id="cerrarsesion"><a href="#" >Salir</a></li>
                      </ul>
                    </li>

                    <?php

                        }
                        else{ 
                    ?>
                    <li><a href="#" data-toggle="modal" data-target="#ModalIniciarSesion" id="modalI"><span class="glyphicon glyphicon-user"></span> Iniciar sesion</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#ModalRegistrarse" id="modalR"><span class="glyphicon glyphicon-list-alt"></span>Registrarse</a></li>
                    <?php
                        }

                    ?>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>    

        </div>
        <!-- Button trigger modal -->
<div class="modal fade" id="ModalEstrenos2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog"  >
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
        <h4 class="modal-title" >Detalles de la Pelicula</h4>
      </div>
      <div class="modal-body" id="DetallesdePelicula">
        </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="ModalIniciarSesion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:350px; height:300px;">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Autentificacion</h4>
      </div>
      <div class="modal-body">

            <center>
               <form id="forminiciarsesion" method="post" class="form-signin form-horizontal"  onsubmit="return false;">  
                       <div id="alertas"> <div class="alert alert-info">Ingrese Los datos</div></div>  
                       <div class="form-group">
                          <label class="col-sm-3 control-label">Email</label>
                          <div class="col-sm-9">
                                <input id="temail" type="text" class="form-control"  name="email"  >

                          </div>
                      </div> 
                      <div class="form-group">
                          <label class="col-sm-3 control-label">Contraseña</label>
                          <div class="col-sm-9">
                              <input  id="tpass" type="password" class="form-control" name="pass" >                 
                               
                          </div>
                      </div>                        
                      <div class="form-group">
                            <div class="col-sm-5 col-sm-offset-3">
                                  <button type="button" class="btn btn-primary" id="iniciarsesion">Iniciar Sesion</button>

                            </div>
                        </div>
                     
                 </form>
               
             </center>
      </div>
    
      
    </div>
  </div>
</div>


<div class="modal fade" id="ModalRegistrarse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog"  >
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Registro</h4>
      </div>
      <div class="modal-body">
            <center>
            <form class="form-signin" role="form" onsubmit="return false;">                 
                <div class="alert alert-info" role="alert">
                  <a href="#" class="alert-link">Datos de Registro</a>
                </div>
                <div id="alerta">
                    <div class="alert alert-warning" role="alert">
                         <a href="#" class="alert-link">Escriba todos los datos</a>
                    </div>
                </div>
                
                 <hr>
                 <div class="row">
                     <div class="col-md-6 col-xs-6 col-sm-6">                 
                         <div class="form-group">
                             <label >Correo</label>
                             <input type="text" class="form-control" id="tcorreo" >  
                        </div>
                        <div class="form-group">
                             <label >Contraseña</label>
                             <input type="password" class="form-control" id="tpass2" >  
                        </div>
                        <div class="form-group">
                             <label >Confirmar Contraseña</label>
                             <input type="password" class="form-control" id="tpass3" >  
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 col-sm-6">
                 
                         <div class="form-group">
                             <label >Pregunta Secreta</label>
                             <input type="text" class="form-control" id="tP" >  
                        </div>
                        <div class="form-group">
                             <label >Respuesta Secreta</label>
                             <input type="text" class="form-control" id="tR" >  
                        </div>
                    </div>
                </div>
                
                <div class="alert alert-info" role="alert">
                  <a href="#" class="alert-link">Datos Personales</a>
                </div>
                <hr>
                
                <div class="row">
                    <div class="col-md-6 col-xs-6 col-sm-6">
                        <div class="form-group">
                             <label >Nombre</label>
                             <input type="text" class="form-control" id="tNombre" >  
                        </div>
                        <div class="form-group">
                             <label >Apellido</label>
                             <input type="text" class="form-control" id="tApellido" >  
                        </div>
                        <div class="form-inline">
                            <label for="tipoci"><h5>Fecha de Nacimiento: </h5></label>
                            <input type="date" class="form-control" id="fecha">
                        </div>
                        <div class="form-group">
                             <label >Pais</label>
                             <input type="text" class="form-control" id="tPais">  
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 col-sm-6">
                        <div class="form-group">
                             <label >Estados</label>
                             <select  id="estados" class="form-control">                
                                <option>Amazonas</option>                                
                                <option>Anzoategui</option>                                
                                <option>Apure</option>                                
                                <option>Aragua</option>                                
                                <option>Area Metropolitana</option>                                
                                <option>Barinas</option>                                
                                <option>Bolivar</option>                                
                                <option>Carabobo</option>                                
                                <option>Cojedes</option>                                
                                <option>Delta Amacuro</option>                                
                                <option>Guárico</option>                                
                                <option>Lara</option>                                
                                <option>Mérida</option>                                
                                <option>Miranda</option>                                
                                <option>Monagas</option>                               
                                <option>Nueva Esparta</option>                                
                                <option>Portuguesa</option>                                
                                <option>Sucre</option>                                
                                <option>Táchira</option>                                
                                <option>Trujillo</option>                                
                                <option>Vargas</option>                                
                                <option>Yaracuy</option>                                
                                <option>Zulia</option>                                
                            </select>
                        </div>
                        <div class="form-group">
                             <label >Sexo </label>
                            <select id="sex" class="form-control" >
                                <option value="F">Femenino</option>
                                <option value="M">Masculino</option>
                                <option value="R" selected="selected">Reservado</option>
                            </select> 

                        </div>
                        <div class="form-group">
                             <label >Direccion: </label>
                             <input type="text" class="form-control" id="tDir" >  
                        </div>
                        <div class="form-group">
                             <label >Telefono: </label>
                             <input type="text" class="form-control" id="tTelf" >  
                        </div>

                    </div>    
                </div>
              </form>
        </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="cancelar"><span class="glyphicon glyphicon-ban-circle"></span> Cancelar</button>
        <button type="button" class="btn btn-primary" id="registrarse">Registrase</button>
      </div>
    </div>
  </div>
</div>
<div class="container" id="contenedor">
    <div id="carousel-example-generic" id="carusel"class="carousel slide" data-ride="carousel">



    <div class="carousel-inner" role="listbox" id="listaPeliculasSlider">

    </div>

    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

    <footer  class="footer">
      <center>
          <img src="img/logo2.png" style="width:50px; height:50px; " >  
          <label > 
            <a href="index.html" title="Inicio">Inicio</a>| 
            <a href="#" id="mlaempresa" title="La Empresa">Proximos Estrenos</a>| 
            <a href="#" id="mservicios" title="Servicios">Cartelera</a>|            
            <a href="#" id="mcontacto" title="Contacto">Contacto</a>
          </label>

          <div id="Social" >
            <b> Todos los derechos reservados @2015    </b>        
          </div>
        </center>
      </footer>

    <script src="js/jquery-2.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
    <script type="text/javascript" src="js/bootstrapValidator.js"></script>
  <script src="js/bootstrapValidator.min.js"></script>
  <script type="text/javascript"> 
   

    $('#forminiciarsesion')
          .bootstrapValidator({
              feedbackIcons: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'El campo de email no puede estar vacio'
                        },
                        emailAddress: {
                            message: 'Es invalido el email'
                        }
                    }
                },
                  pass: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  }

              }
          }).on('error.field.bv', function(e, data) {
              data.bv.disableSubmitButtons(false);
          }).on('success.field.bv', function(e, data) {
              data.bv.disableSubmitButtons(false);
          });






    </script>
    <script type="text/javascript" src="js/eventosinicio.js"></script>
    <script type="text/javascript">
        inicio();
    </script>
    
</body>
</html>







